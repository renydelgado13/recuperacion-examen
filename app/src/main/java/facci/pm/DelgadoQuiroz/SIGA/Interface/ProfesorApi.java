package facci.pm.DelgadoQuiroz.SIGA.Interface;

import java.util.List;

import facci.pm.DelgadoQuiroz.SIGA.Model.Estudiantes;
import facci.pm.DelgadoQuiroz.SIGA.Model.Profesor;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProfesorApi {

    @GET("teacher/{cedula}")
    Call<Profesor> getProfesor(@Path("cedula") String profesor);

    @GET("student")
    Call<List<Estudiantes>> getEstudiantes();

}
