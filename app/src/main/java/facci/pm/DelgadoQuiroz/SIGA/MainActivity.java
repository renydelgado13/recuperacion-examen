package facci.pm.DelgadoQuiroz.SIGA;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button buttonPrefesor, buttonEstudiantes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);
        buttonPrefesor = findViewById(R.id.btnprofe);
        buttonEstudiantes = findViewById(R.id.btnStudiente);

        buttonPrefesor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DatosProfe.class);
                intent.putExtra("cedula", editText.getText().toString());
                startActivity(intent);

            }
        });

        buttonEstudiantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EstudiantesProfe.class);
                //intent.putExtra("cedula", editText.getText().toString());
                startActivity(intent);
            }
        });

    }
}
