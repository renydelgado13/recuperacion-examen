package facci.pm.DelgadoQuiroz.SIGA;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import facci.pm.DelgadoQuiroz.SIGA.Interface.ProfesorApi;
import facci.pm.DelgadoQuiroz.SIGA.Model.Estudiantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EstudiantesProfe extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiantes_profe);

        textView = findViewById(R.id.jsonText2);

        getEstudiantes();
    }

    private void getEstudiantes() {

        Retrofit retrofit =new Retrofit.Builder().baseUrl("https://backend-posts.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProfesorApi profesorApi = retrofit.create(ProfesorApi.class);

        Call<List<Estudiantes>> call = profesorApi.getEstudiantes();
        call.enqueue(new Callback<List<Estudiantes>>() {
            @Override
            public void onResponse(Call<List<Estudiantes>> call, Response<List<Estudiantes>> response) {

                if (!response.isSuccessful()){
                    textView.setText("Codigo: " +response.code());
                    return;

                }

                List<Estudiantes> estList =response.body();

                for (Estudiantes estudiantes: estList){

                    String content = "";
                    content += "Cedula: " + estudiantes.getCedula() + "\n";
                    content += "Ciudad: " + estudiantes.getCiudad() + "\n";
                    content += "Pais: " + estudiantes.getPais() + "\n";
                    content += "Descripcion: " + estudiantes.getDescripcion() + "\n";
                    content += "Nombre: " + estudiantes.getNombre() + "\n";
                    content += "Apellido: " + estudiantes.getApellido() + "\n\n";
                    textView.append(content);

                }
            }

            @Override
            public void onFailure(Call<List<Estudiantes>> call, Throwable t) {
                textView.setText(t.getMessage());

            }
        });
    }
}
