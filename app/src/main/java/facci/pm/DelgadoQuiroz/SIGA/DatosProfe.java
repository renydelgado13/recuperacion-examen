package facci.pm.DelgadoQuiroz.SIGA;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import facci.pm.DelgadoQuiroz.SIGA.Interface.ProfesorApi;
import facci.pm.DelgadoQuiroz.SIGA.Model.Profesor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DatosProfe extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_profe);

        textView = findViewById(R.id.jsonText);

        String cedulaProfe =getIntent().getStringExtra("cedula");

        mostrarProfesor(cedulaProfe);
    }

    private void mostrarProfesor(String cedulaProfe) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://backend-posts.herokuapp.com/").addConverterFactory(GsonConverterFactory.create())
                .build();

        ProfesorApi profesorApi = retrofit.create(ProfesorApi.class);

        Call<Profesor> call = profesorApi.getProfesor(cedulaProfe);

        call.enqueue(new Callback<Profesor>() {
            @Override
            public void onResponse(Call<Profesor> call, Response<Profesor> response) {

                Profesor profesor = response.body();

                String content = "";
                content += "Cedula: " + profesor.getCedula() + "\n";
                content += "Ciudad: " + profesor.getCiudad() + "\n";
                content += "Biografia: " + profesor.getBiografia() + "\n";
                content += "Nombre: " + profesor.getNombre() + "\n";
                content += "Apellido: " + profesor.getApellido();

                textView.append(content);

            }

            @Override
            public void onFailure(Call<Profesor> call, Throwable t) {
                textView.setText(t.getMessage());

            }
        });
    }
}
