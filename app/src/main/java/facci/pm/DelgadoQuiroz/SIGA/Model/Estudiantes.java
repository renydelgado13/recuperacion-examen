package facci.pm.DelgadoQuiroz.SIGA.Model;

public class Estudiantes {

    private String cedula;
    private String ciudad;
    private String pais;
    private String descripcion;
    private String nombre;
    private String apellido;


    public String getCedula() {
        return cedula;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getPais() {
        return pais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
}
