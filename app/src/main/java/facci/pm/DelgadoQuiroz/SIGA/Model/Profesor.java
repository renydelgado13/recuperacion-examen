package facci.pm.DelgadoQuiroz.SIGA.Model;

public class Profesor {

    private String cedula;
    private String ciudad;
    private String biografia;
    private String nombre;
    private String apellido;


    public String getCedula() {
        return cedula;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getBiografia() {
        return biografia;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
}
